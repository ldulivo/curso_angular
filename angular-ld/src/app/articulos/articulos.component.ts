import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { ArticuloLst } from './../models/articulos.model';

@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.component.html',
  styleUrls: ['./articulos.component.css']
})
export class ArticulosComponent implements OnInit {
  @Input() articulo: ArticuloLst;
  @HostBinding('attr.class') cssClass = 'col-md-4 list-group';

  constructor() {    
  }

  ngOnInit(): void {
  }

}
