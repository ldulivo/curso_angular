import { Component, OnInit } from '@angular/core';
import { ArticuloLst } from './../models/articulos.model';

@Component({
  selector: 'app-articulos-lista',
  templateUrl: './articulos-lista.component.html',
  styleUrls: ['./articulos-lista.component.css']
})
export class ArticulosListaComponent implements OnInit {
  articulos: ArticuloLst[];

  constructor() {
    this.articulos = [];
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, precio:string, cantidad:string):boolean {
    this.articulos.push(new ArticuloLst(nombre, precio, cantidad));
    return false;
  }

}
