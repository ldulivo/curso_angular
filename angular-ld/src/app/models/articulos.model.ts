export class ArticuloLst {
    nombre:string;
    precio:string;
    cantidad:string;

    constructor(n:string, p:string, c:string) {
        this.nombre = n;
        this.precio = p;
        this.cantidad = c;
    }
}