import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ArticulosComponent } from './articulos/articulos.component';
import { ArticulosListaComponent } from './articulos-lista/articulos-lista.component';

@NgModule({
  declarations: [
    AppComponent,
    ArticulosComponent,
    ArticulosListaComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
